#!/usr/bin/env python3

import venv
import os
import os.path
import sys

this_dir = os.path.dirname(os.path.realpath(__file__))
env_dir = os.path.join(this_dir, 'run')
venv.create(env_dir, symlinks=True, clear=True, with_pip=False)
GET_PIP_VERSION = "1.5.4"
GET_PIP_URL = "https://raw.githubusercontent.com/pypa/pip/{0}/contrib/get-pip.py".format(GET_PIP_VERSION)
GET_PIP_SHASUM = "121840cfc8723716be1c9711d340e80245a31995e6e0861e3f27237848c065c8"

pip_installed = False
try:
	import ensurepip
	ensurepip.bootstrap(root=env_dir, default_pip=True)
	pip_installed = True
except ImportError as error:
	print("module ensurepip not present; attempting to download; {0!r}".format(sys.exc_info()))

if not pip_installed:
	import urllib.request
	getpip_dir = os.path.join(env_dir, 'tmp-packages')
	os.mkdir(getpip_dir)

	with urllib.request.urlopen(GET_PIP_URL) as response:
		with open(os.path.join(getpip_dir, "get-pip.py"), "wb") as getpip_file:
			print("Downloading...")
			getpip_file.write(response.read())
	import hashlib
	with open(os.path.join(getpip_dir, "get-pip.py"), "rb") as getpip_file:
		print("Calculating hash...")
		shasum = hashlib.sha256(getpip_file.read()).hexdigest()
	assert shasum == GET_PIP_SHASUM, "Downloaded hash of get-pip.py doesn't match;\n  {0:<10} {1}\n  {2:<10} {3}".format("expected", GET_PIP_SHASUM, "actual", shasum)
	
	import subprocess as proc
	pip_install_command = [os.path.join(env_dir, 'bin', 'python'), os.path.join(getpip_dir, 'get-pip.py')]
	print("Executing {0!r}...".format(pip_install_command))
	proc.check_call(pip_install_command)
## EOF

