#!/usr/bin/env python3

"""
Pyscaffic is for people who need a quick and easy way to create
presentation style videos; where the screen commands most of the attention
but the presenter is not neglected.

This format is typically seen in most conference (CON) presentations.

This program is being made available under the Apache License, version 2.
"""

## EOF
