#!/usr/bin/env python3

import shutil

tk_true = 1
tk_false = 0

conf = {}

def pack(widget, **kw):
	widget.pack(**kw)
	return widget
#

def grid(widget, **kw):
	widget.grid(**kw)
	return widget
#

def get_command_to_find_exe_in_path(exe_name, conf_var):
	def find_exe_in_path():
		conf_var.set("")
		exe_path = shutil.which(exe_name)
		if exe_path:
			conf_var.set(exe_path)
	#
	return find_exe_in_path
#

##
