#!/usr/bin/env python3


from   collections import namedtuple
import inspect
import itertools

import tkinter as tk
from   tkinter import ttk

import pyscaffic
import pyscaffic.ui.util as uiu

class AboutFrame(ttk.Frame):
	def __init__(self, parent, *args, **kw):
		super().__init__(parent)
		uiu.pack(ttk.Label(self, text=inspect.cleandoc(pyscaffic.__doc__)))
	#
#

class AddMediaFrame(ttk.Frame):
	def __init__(self, parent, *args, **kw):
		super().__init__(parent)
	#

#

class DriversFrame(ttk.Labelframe):
	def __init__(self, parent, *args, **kw):
		super().__init__(parent, text="Drivers")
		uiu.pack(ttk.Label(self, text="Drivers are not just for cars anymore"), fill="x", expand=uiu.tk_true)
	#

#

class ExecutablesFrame(ttk.Labelframe):
	def __init__(self, parent, *args, **kw):
		super().__init__(parent, text="Executables")
		for (index, tool_name) in zip(itertools.count(1), [ 'FFMPEG', 'MediaInfo' ]):
			uiu.grid(ttk.Label(self, text=tool_name+":"), row=index, column=1, padx=3, pady=3)
		for (index, conf_name) in zip(itertools.count(1), [ 'ffmpeg', 'mediainfo' ]):
			uiu.grid(ExecutableToolPathFrame(self, conf_name, conf_name), row=index, column=2, sticky="ew", padx=3, pady=3)
		self.columnconfigure(2, weight=1)
	#
#

class ExecutableToolPathFrame(ttk.Frame):
	def __init__(self, parent, conf_name, exe_name, *args, **kw):
		super().__init__(parent)
		conf_key = 'tool.' + conf_name + '.path'
		uiu.conf[conf_key] = tk.StringVar()
		uiu.pack(ttk.Entry(self, textvariable=uiu.conf[conf_key]), anchor="w", fill="x", expand=uiu.tk_true)
		Wgt = namedtuple('Widget', ['text', 'command'])
		for button in [
			Wgt(text="Download", command=None),
			Wgt(text="PATH", command=uiu.get_command_to_find_exe_in_path(exe_name, uiu.conf[conf_key])),
			Wgt(text="Browse ...", command=None)
		]:
			uiu.pack(ttk.Button(self, text=button.text, command=button.command), side="left", anchor="w")
	#
	
#

class MainFrame(ttk.Notebook):
	
	def __init__(self, parent, *args, **kw):
		super().__init__(parent)
		
		Wgt = namedtuple('Widget', ['cls','text'])
		for frame in [ 
				Wgt(cls=AddMediaFrame, text="Add Media"),
				Wgt(cls=PreviewFrame, text="Preview"),
				Wgt(cls=ToolsFrame, text="Tools"),
				Wgt(cls=AboutFrame, text="About")
		]:
			self.add(uiu.pack(frame.cls(self), fill="both", expand=uiu.tk_true), text=frame.text)
	#
#

class PreviewFrame(ttk.Frame):
	def __init__(self, parent, *args, **kw):
		super().__init__(parent)
	#
#

class ToolsFrame(ttk.Frame):
	def __init__(self, parent, *args, **kw):
		super().__init__(parent)
		uiu.pack(ExecutablesFrame(self), fill="x", expand=uiu.tk_true, anchor="n")
		uiu.pack(DriversFrame(self), fill="x", expand=uiu.tk_true, anchor="n")
	#

#

## EOF

