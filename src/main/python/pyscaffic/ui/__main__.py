#!/usr/bin/env python3

import tkinter as tk

import pyscaffic.ui.util as uiu
from   pyscaffic.ui.widgets import MainFrame

root = tk.Tk()
root.title("Pyscaffic!")
MainFrame(root).pack(fill="both", expand=uiu.tk_true)
root.mainloop()

##
