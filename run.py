#!/usr/bin/env python3

import sys
import os.path
import importlib

this_dir = os.path.dirname(os.path.realpath(__file__))
mod_dir = os.path.join(this_dir, 'src', 'main', 'python')
while (this_dir in sys.path):
	sys.path.remove(this_dir)
sys.path.insert(0, mod_dir)

from pyscaffic.ui import __main__

##
